package com.shuke.shopsmall.shifou.common;

/**
 * Created by Administrator on 2015/8/11.
 * 请求协议常量
 */
public class ProtocolConstant {
    public static final String PARA_STARTUP_PICTURE = "startup_picture";
    public static final String PARA_PROCESS_TIME = "processtime";
}
