package com.shuke.shopsmall.shifou.common;

import android.os.Environment;

/**
 * Created by Administrator on 2015/8/11.
 */
public class Constant {

    public static final String DESK_SHAREPREFERENCES_FILE = "desk";

    public static final class Path{
        public final static String SDCARD = Environment.getExternalStorageDirectory().getPath();
        public static String sSHIFOU_DIR = SDCARD + "/ShiFou";

        //启动页存放路径
        public static final String START_UP_IMAGE_PATH = SDCARD + "/ShiFou/download/StartUpPicture/";

    }

}
