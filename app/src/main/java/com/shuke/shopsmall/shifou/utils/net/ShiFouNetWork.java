package com.shuke.shopsmall.shifou.utils.net;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.Volley;
import com.shuke.shopsmall.shifou.common.ErrorCode;
import com.shuke.shopsmall.shifou.common.NetConstant;
import com.shuke.shopsmall.shifou.common.ProtocolConstant;
import com.shuke.shopsmall.shifou.utils.NetUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2015/8/11.
 * 所有网络数据请求都在这里实现
 */
public class ShiFouNetWork {
    private RequestQueue mRequestQueue;
    private Context mContext;

    public ShiFouNetWork(Context context) {
        super();
        mRequestQueue = Volley.newRequestQueue(context);
        mContext = context;
    }

    /**
     * 网路请求:发送一个POST请求，请求响应返回的是一个JsonObject
     */
    public void sendPostJsonObjectRequest(String url, final Map<String, String> param,
                                          final ShiFouNetworkCallback callback) {
        Log.d("zou", "<sendPostJsonObjectRequest> request url = " + url + " param = " + param);
        Request<JSONObject> request = new NormalPostRequest(url, new Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                Log.d("zou", "<sendPostJsonObjectRequest> jsonObject=" + jsonObject);
                callback.onSuccess(jsonObject);
            }
        }, new ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                if (!NetUtils.isNetworkOK(mContext)) {
                    callback.onFail(ErrorCode.ERROR_CODE_NO_NETWORK);
                } else {
                    callback.onFail(ErrorCode.ERROR_CODE_TIMEOUT);
                }
            }
        }, param);

        request.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        mRequestQueue.add(request);
    }


    public JSONObject getPhead() {

        return null;
    }


    /**
     * 启动页面
     ***/
    public void getLauncherImage(String lastTime, ShiFouNetworkCallback callback) {
//        String url = NetConstant.getFullUrl(NetConstant.GOMARKET_SPLASH_SCREEN_URL);
//        Map<String, String> param = new HashMap<String, String>();
//        JSONObject jsonObject = new JSONObject();
//        try {
//            jsonObject.put(ProtocolConstant.PARA_P_HEAD, getPhead());
//            jsonObject.put(ProtocolConstant.PARA_LAST_ACCESS_TIME, lastTime);
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        param.put(ProtocolConstant.PARA_DATA, jsonObject.toString());
//        sendPostJsonObjectRequest(url, param, callback);
    }

    /**
     * <br>类描述:普通POST请求
     * <br>功能详细描述:以键值对的方式提交post参数
     */
    private class NormalPostRequest extends Request<JSONObject> {
        private Map<String, String> mMap;
        private Listener<JSONObject> mListener;

        public NormalPostRequest(String url, Listener<JSONObject> listener, ErrorListener errorListener, Map<String, String> map) {
            super(Request.Method.POST, url, errorListener);

            mListener = listener;
            mMap = map;
        }

        //mMap是已经按照前面的方式,设置了参数的实例
        @Override
        protected Map<String, String> getParams() throws AuthFailureError {
            return mMap;
        }

        //此处因为response返回值需要json数据,和JsonObjectRequest类一样即可
        @Override
        protected Response<JSONObject> parseNetworkResponse(NetworkResponse networkResponse) {
            try {
                String jsonString = new String(networkResponse.data,
                        HttpHeaderParser.parseCharset(networkResponse.headers));

                return Response.success(new JSONObject(jsonString),
                        HttpHeaderParser.parseCacheHeaders(networkResponse));
            } catch (UnsupportedEncodingException e) {
                return Response.error(new ParseError(e));
            } catch (JSONException je) {
                return Response.error(new ParseError(je));
            }
        }

        @Override
        protected void deliverResponse(JSONObject jsonObject) {
            mListener.onResponse(jsonObject);
        }
    }

    public void cancel() {
        mRequestQueue.cancelAll(new RequestQueue.RequestFilter() {
            @Override
            public boolean apply(Request<?> request) {
                return true;
            }
        });
    }
}
