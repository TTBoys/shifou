package com.shuke.shopsmall.shifou.utils;

import android.graphics.Bitmap;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;

import android.graphics.Bitmap.CompressFormat;

/**
 * Created by Administrator on 2015/8/11.
 */
public class BitmapUtil {
    private final static String TAG = "zou";

    /**
     * 功能简述:将位图保存为指定文件名的文件。
     * 功能详细描述:
     * 注意:若已存在同名文件，则首先删除原有文件，若删除失败，则直接退出，保存失败
     *
     * @param bmp：待保存位图
     * @param bmpName：保存位图内容的目标文件路径
     * @return true for 保存成功，false for 保存失败。
     */
    public static final boolean saveBitmap(Bitmap bmp, String bmpName) {
        return saveBitmap(bmp, bmpName, Bitmap.CompressFormat.PNG);
    }

    public static final boolean saveBitmap(Bitmap bmp, String bmpName, CompressFormat format) {
        if (null == bmp) {
            Log.i(TAG, "save bitmap to file bmp is null");
            return false;
        }
        FileOutputStream stream = null;
        try {
            File file = new File(bmpName);
            if (file.exists()) {
                boolean bDel = file.delete();
                if (!bDel) {
                    Log.i(TAG, "delete src file fail");
                    return false;
                }
            } else {
                File parent = file.getParentFile();
                if (null == parent) {
                    Log.i(TAG, "get bmpName parent file fail");
                    return false;
                }
                if (!parent.exists()) {
                    boolean bDir = parent.mkdirs();
                    if (!bDir) {
                        Log.i(TAG, "make dir fail");
                        return false;
                    }
                }
            }
            boolean bCreate = file.createNewFile();
            if (!bCreate) {
                Log.i(TAG, "create file fail");
                return false;
            }
            stream = new FileOutputStream(file);
            boolean bOk = bmp.compress(format, 100, stream);

            if (!bOk) {
                Log.i(TAG, "bitmap compress file fail");
                return false;
            }
        } catch (Exception e) {
            Log.i(TAG, e.toString());
            return false;
        } finally {
            if (null != stream) {
                try {
                    stream.close();
                } catch (Exception e2) {
                    Log.i(TAG, "close stream " + e2.toString());
                }
            }
        }
        return true;
    }
}
