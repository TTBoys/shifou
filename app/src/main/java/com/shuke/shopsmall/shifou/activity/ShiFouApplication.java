//
//                            _ooOoo_
//                           o8888888o
//                           88" . "88
//                           (| -_- |)
//                            O\ = /O
//                        ____/`---'\____
//                      .   ' \\| |// `.
//                       / \\||| : |||// \
//                     / _||||| -:- |||||- \
//                       | | \\\ - /// | |
//                     | \_| ''\---/'' | |
//                      \ .-\__ `-` ___/-. /
//                   ___`. .' /--.--\ `. . __
//                ."" '< `.___\_<|>_/___.' >'"".
//               | | : `- \`.;`\ _ /`;.`/ - ` : | |
//                 \ \ `-. \_ __\ /__ _/ .-` / /
//         ======`-.____`-.___\_____/___.-`____.-'======
//                            `=---='
//
//         .............................................
//                  佛祖镇楼                  BUG辟易
package com.shuke.shopsmall.shifou.activity;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.shuke.shopsmall.R;
import com.shuke.shopsmall.shifou.Interface.ResultCallBack;
import com.shuke.shopsmall.shifou.common.Constant;
import com.shuke.shopsmall.shifou.common.ErrorCode;
import com.shuke.shopsmall.shifou.common.PreferencesConstant;
import com.shuke.shopsmall.shifou.common.ProtocolConstant;
import com.shuke.shopsmall.shifou.manage.DataManager;
import com.shuke.shopsmall.shifou.manage.PreferencesManager;
import com.shuke.shopsmall.shifou.utils.BitmapUtil;
import com.shuke.shopsmall.shifou.utils.imageLoader.AsyncImageLoader;
import com.shuke.shopsmall.shifou.utils.imageLoader.AsyncImageLoaderListener;

import java.io.File;
import java.util.Map;

public class ShiFouApplication extends AppCompatActivity {
    private ImageView mStartupView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shifou_application);

        mStartupView = (ImageView) findViewById(R.id.iv_startup);

        //初始化
        DataManager.getInstance().init(this);

        /***********启动页***********************/
        loadStartPage();
    }

    /**
     * <br>功能简述:启动页
     * <br>功能详细描述:时间 5秒
     * <br>注意:
     */
    private void loadStartPage() {
        final PreferencesManager sharedPreferences = PreferencesManager.getSharedPreference(this,
                PreferencesConstant.SHARE_STARTUP_PAGE_PREFERENCES, Context.MODE_PRIVATE);
        /***********如果本地有图片，从本地获取图片*************/
        String pictureName = sharedPreferences.getString(PreferencesConstant.STARTUP_PAGE_PIC_NAME, null);
        if (pictureName != null && pictureName.length() > 0) {
            String filePath = Constant.Path.START_UP_IMAGE_PATH + pictureName;
            File file = new File(Constant.Path.START_UP_IMAGE_PATH, pictureName);
            if (file.exists()) {
                AsyncImageLoader.getInstance().loadSDImageNoDefaul(filePath, mStartupView);
                new Handler(getMainLooper()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mStartupView.setVisibility(View.GONE);
                    }
                }, 5000); //PreferencesConstant.STARTUP_PAGE_SHOW_TIME
            }
        }

        /**************否则从网络去获取，并存本地*************/
        DataManager.getInstance().getLauncherImage(null, new ResultCallBack() {
            @Override
            public void onSuccess(Map<String, String> result) {
                final String imageUrl = result.get(ProtocolConstant.PARA_STARTUP_PICTURE);
                final String showTime = result.get(ProtocolConstant.PARA_PROCESS_TIME);
                AsyncImageLoader.getInstance().loadImage(imageUrl, new AsyncImageLoaderListener() {
                    @Override
                    public void onLoadingStarted(String arg0, View arg1) {
                    }

                    @Override
                    public void onLoadingFailed(String arg0, View arg1) {
                    }

                    @Override
                    public void onLoadingComplete(String arg0, View arg1, Bitmap arg2) {
                        int index = imageUrl.lastIndexOf("/");
                        String imageName = imageUrl.substring(index + 1);
                        String filePath = Constant.Path.START_UP_IMAGE_PATH + imageName;

                        BitmapUtil.saveBitmap(arg2, filePath, Bitmap.CompressFormat.PNG);
                        sharedPreferences.putString(PreferencesConstant.STARTUP_PAGE_PIC_NAME, imageName);
                        sharedPreferences.putString(PreferencesConstant.STARTUP_PAGE_SHOW_TIME, showTime);
                        sharedPreferences.commit();
                    }

                    @Override
                    public void onLoadingCancelled(String arg0, View arg1) {

                    }
                });
            }

            @Override
            public void onFail(int errorCode) {
                if (errorCode == ErrorCode.ERROR_CODE_NO_NETWORK) {
                    Toast.makeText(getApplicationContext(), "网络未连接！", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), "未拿到启动页面图！", Toast.LENGTH_SHORT).show();

                }
            }
        });

    }
}
