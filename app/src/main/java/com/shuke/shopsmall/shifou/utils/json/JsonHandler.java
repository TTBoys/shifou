package com.shuke.shopsmall.shifou.utils.json;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

/**
 * Created by Administrator on 2015/8/11.
 * json 处理
 */
public class JsonHandler {

    public static JSONObject getRquestHead(Map<String, String> map) {
        JSONObject jsonObject = new JSONObject();
        if (map != null) {
            for (String key : map.keySet()) {
                try {
                    jsonObject.put(key, map.get(key));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        return jsonObject;
    }


}
