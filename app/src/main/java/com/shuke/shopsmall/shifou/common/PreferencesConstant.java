package com.shuke.shopsmall.shifou.common;

/**
 * Created by Administrator on 2015/8/11.
 *
 */
public class PreferencesConstant {

    /*****************启动页*****************/
    public static final String SHARE_STARTUP_PAGE_PREFERENCES = "startup_page_preferences";  //启动页，标记位
    public static final String STARTUP_PAGE_PIC_NAME = "startup_page_pic_name";
    public static final String STARTUP_PAGE_SHOW_TIME = "startup_page_show_time";
}

