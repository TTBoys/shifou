package com.shuke.shopsmall.shifou.manage;

import android.content.Context;

import com.shuke.shopsmall.shifou.Interface.ResultCallBack;
import com.shuke.shopsmall.shifou.utils.net.ShiFouNetWork;
import com.shuke.shopsmall.shifou.utils.net.ShiFouNetworkCallback;

import org.json.JSONObject;

/**
 * Created by Administrator on 2015/8/11.
 * 数据处理管理类
 * 所有数据管理都在这里处理，包括读写缓存，请求网络数据，临时数据处理
 */
public class DataManager {
    private Context mContext;
    private ShiFouNetWork mShiFouNetWork;

    private static DataManager sInstance;

    private DataManager() {

    }

    public synchronized static DataManager getInstance() {
        if (sInstance == null) {
            sInstance = new DataManager();
        }
        return sInstance;
    }

    public void init(Context context) {
        mContext = context;
        mShiFouNetWork = new ShiFouNetWork(context);

    }

    //启动页
    public void getLauncherImage(String lastTime, final ResultCallBack callBack) {
        mShiFouNetWork.getLauncherImage(lastTime, new ShiFouNetworkCallback() {
            @Override
            public void onSuccess(JSONObject jsonObject) {
            //TODO: json处理

            }

            @Override
            public void onFail(int errorCode) {
                callBack.onFail(errorCode);
            }
        });
    }
}