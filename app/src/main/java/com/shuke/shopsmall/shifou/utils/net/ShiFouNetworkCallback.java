package com.shuke.shopsmall.shifou.utils.net;

import org.json.JSONObject;

/**
 * Created by Administrator on 2015/8/11.
 * 网络请求回调接口
 */
public interface ShiFouNetworkCallback {
    public void onSuccess(JSONObject jsonObject);
    public void onFail(int errorCode);
}
