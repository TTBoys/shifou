package com.shuke.shopsmall.shifou.utils;

import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by Administrator on 2015/8/11.
 */
public class AppUtils {
    /**
     * 检查是否安装某包
     */
    public static boolean isAppExist(final Context context, final String packageName) {
        if (context == null || packageName == null) {
            return false;
        }
        boolean result = false;
        try {
            context.getPackageManager().getPackageInfo(packageName,
                    PackageManager.GET_SHARED_LIBRARY_FILES);
            result = true;
        } catch (PackageManager.NameNotFoundException e) {
            result = false;
        } catch (Exception e) {
            result = false;
        }
        return result;
    }


}
