package com.shuke.shopsmall.shifou.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

/**
 * Created by Administrator on 2015/8/11.
 * 基类
 */
public class BaseActivity extends FragmentActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        if (intent != null) {
            Log.i("zou", "<BaseActivity> onCreate");
        }
    }

    @Override
    protected void onPause() {
//		TransitionAnimController.getInstance().overridePauseTransitionAnim(this);
        super.onPause();
    }

    @Override
    protected void onRestart() {
//		TransitionAnimController.getInstance().overrideRestartTransitionAnim(this);
        super.onRestart();
    }

    @Override
    protected void onResume() {
//		TransitionAnimController.getInstance().overrideResumeTransitionAnim(this);
        super.onResume();
    }

    @Override
    protected void onStop() {
//		TransitionAnimController.getInstance().overrideStopTransitionAnim(this);
        super.onStop();
    }

    @Override
    protected void onDestroy() {
//		DataManager.getInstance().cancel();
        super.onDestroy();

    }

}
