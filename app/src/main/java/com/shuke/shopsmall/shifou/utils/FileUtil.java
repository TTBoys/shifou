package com.shuke.shopsmall.shifou.utils;

import android.os.Environment;

/**
 * Created by Administrator on 2015/8/11.
 */
public class FileUtil {

    /**
     * sd卡是否可读写
     */
    public static boolean isSDCardAvaiable() {
        return Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState());
    }
}
