package com.shuke.shopsmall.shifou.Interface;

import java.util.Map;

/**
 * Created by Administrator on 2015/8/11.
 */
public interface ResultCallBack {

    public void onSuccess(Map<String, String> result);
    public void onFail(int errorCode);
}
