package com.shuke.shopsmall.shifou.activity;

import android.annotation.TargetApi;
import android.app.Fragment;
import android.os.Build;

import java.lang.reflect.Field;

/**
 * Created by Administrator on 2015/8/11.
 */
public abstract class BaseFragment extends Fragment {
    protected boolean mIsVisible;

    protected void onInvisible() {
    }

    protected void onVisible() {
    }

    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1)
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (getUserVisibleHint()) {
            mIsVisible = true;
            onVisible();
        } else {
            mIsVisible = false;
            onInvisible();
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);
        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }
}
